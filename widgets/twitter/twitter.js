widget = {

  onData: function(el, data) {
    if (data.title) {
      $('h2', el).text(data.title);
    }

    tweets = data.feed;

    $('.content', el).empty();

    for (var i = 0; i < tweets.length;  i++) {
      $('.content', el).append(
          "<blockquote>" + tweets[i].text + "<cite>" + tweets[i].user.screen_name  + "</cite></blockquote>"
      );
    }
  }
};
