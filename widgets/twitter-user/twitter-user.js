widget = {

  onData: function (el, data) {
    /*
     * JavaScript Pretty Date
     * Copyright (c) 2011 John Resig (ejohn.org)
     * Licensed under the MIT and GPL licenses.
     */

// Takes an ISO time and returns a string representing how
// long ago the date represents.
    function prettyDate(time) {
      var date = new Date((time || "").replace(/-/g, "/").replace(/[TZ]/g, " ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

      if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
        return;

      return day_diff == 0 && (
        diff < 60 && "just now" ||
        diff < 120 && "1 minute ago" ||
        diff < 3600 && Math.floor(diff / 60) + " minutes ago" ||
        diff < 7200 && "1 hour ago" ||
        diff < 86400 && Math.floor(diff / 3600) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
    }

// If jQuery is included in the page, adds a jQuery plugin to handle it as well
    if (typeof jQuery != "undefined")
      jQuery.fn.prettyDate = function () {
        return this.each(function () {
          var date = prettyDate(this.title);
          if (date)
            jQuery(this).text(date);
        });
      };
    var tweets = data.feed;
    var image = tweets[0]["user"]["profile_image_url"];
    // image = image.replace('normal', '200x200');
    var screenName = tweets[0].user.screen_name;

    if (data.title) {
      $('h2', el).text(data.title + " - @" + screenName);
    }

    $('.imgcontent', el).empty();

    $('.imgcontent', el).append(
      "<a href='http://twitter.com/'" + screenName + "><img src=\"" + image + "\" />" +
      "</a>"
    );
    // $('.overcontent', el).css('background-image',  "url('" + image + "')");

    $('.content', el).empty();
    for (var i = 0; i < tweets.length; i++) {
      $('.content', el).append(
        "<cite>" + prettyDate(tweets[i].created_at) + "</cite>" +
        "<blockquote>" + tweets[i].text + "</blockquote>"
      );
    }
  }
};
