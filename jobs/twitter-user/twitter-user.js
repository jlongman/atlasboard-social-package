module.exports = function(config, dependencies, job_callback) {
  var logger = dependencies.logger;
  var error_message = "";

  /* Return error if no twitterUser parameter is configured */
  if (!config.twitterUser){
    error_message = 'no user parameter found';
    logger.error(error_message);

    return job_callback(error_message);
  }

  /* Return error if no auth parameters are configured */
  if (!config.globalAuth || !config.globalAuth.twitter.consumer_key ||
      !config.globalAuth.twitter.consumer_secret || !config.globalAuth.twitter.access_token ||
      !config.globalAuth.twitter.access_token_secret) {
    error_message = 'no authentication info available';
    logger.error(error_message);

    return job_callback(error_message);
  }

  var Twit = require('twit');

  /* Set credentials from globalAuth.json file */
  var T = new Twit({
    consumer_key: config.globalAuth.twitter.consumer_key,
    consumer_secret: config.globalAuth.twitter.consumer_secret,
    access_token: config.globalAuth.twitter.access_token,
    access_token_secret: config.globalAuth.twitter.access_token_secret
  });

  /* Set 5 as default number of tweets to fetch */
  number_of_tweets = config.numberOfTweets ? config.numberOfTweets : 5;
  username = encodeURIComponent(config.twitterUser);

  /* Query the Twitter API */
  T.get('statuses/user_timeline', { screen_name: username, count: number_of_tweets + 5, exclude_replies: "true"}, function(err, data, response) {
    if (err || !response || response.statusCode != 200) {
      error_message = err || (response ? ("bad statusCode: " + response.statusCode + " from " + options.url) : ("bad response from " + options.url));
      logger.error(error_message);
      job_callback(error_message);
    } else {
      if (data.length > number_of_tweets) {
         data = data.slice(0, number_of_tweets);
      }
      job_callback(null, {feed: data, title: config.twitterTitle});
    }
  });
};
